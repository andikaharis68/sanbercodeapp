import React, { createContext, useState } from 'react'
import { View, Text } from 'react-native'
import TodoList from './TodoList'

export const RootContext = createContext()

const Context = () => {
    const [value, setValue] = useState('');
    const [nomor, setNomor] = useState('')
    const [todos, setTodos] = useState([]);

    const handleChangeInputName = (value) => {
        setValue(value)
    }
    const handleChangeInputNomor = (nomor) => {
        setNomor(nomor)
    }

    const addTodo = () => {
        setTodos([...todos, { text: value, number: nomor, key: Math.random().toString() }]);
        setValue('');
        setNomor('');
    };
    const deleteTodo = (id) => {
        setTodos(
            todos.filter(todo => {
                if (todo.key != id) return true;
            })
        );
    };
    return (
        <RootContext.Provider value={{
            value,
            nomor,
            todos,
            handleChangeInputName,
            handleChangeInputNomor,
            deleteTodo,
            addTodo,

        }}>
            <TodoList />
        </RootContext.Provider>
    )
}

export default Context