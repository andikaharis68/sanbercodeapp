import React, { useContext } from 'react'
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, StatusBar, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import Trash from '../src/assets/icons/trash.svg'
import Folder from '../src/assets/icons/folder.svg'
import { RootContext } from './index'
import Profile from '../src/assets/icons/profile.svg'

const TodoList = () => {

    const state = useContext(RootContext)

    const Item = ({ text, number, id }) => {
        return (
            <View style={{ flexDirection: 'row', height: 65, borderWidth: 3, borderColor: '#F97794', borderRadius: 5, marginTop: 10, paddingHorizontal: 15, alignItems: 'center' }} >
                <View style={{ flex: 1 }} >
                    <Text style={{ fontWeight: "bold", color: 'white', fontSize: 15 }}>{text}</Text>
                    <Text style={{ color: 'white' }}>{number}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={() => state.deleteTodo(id)} >
                        <Trash heigh={30} width={30} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient colors={['#F97794', '#623AA2']} style={{ flex: 1 }}>
                <StatusBar backgroundColor="#F97794" />
                <View style={{ marginHorizontal: 10, marginTop: 20 }}>
                    <View style={{ backgroundColor: 'pink', padding: 20, borderBottomRightRadius: 15, borderBottomLeftRadius: 15 }}>
                        <Text style={{ fontSize: 18, textAlign: 'center' }}>Buat kontak baru</Text>
                        <View style={{ alignItems: 'center', marginTop: 20 }}>
                            <Profile width={65} height={65} />
                        </View>
                        <View style={{ marginVertical: 20 }}>
                            <View >
                                <TextInput placeholder='Masukan nama' style={{ borderBottomWidth: 2, paddingLeft: 10 }}
                                    value={state.value}
                                    onChangeText={value => state.handleChangeInputName(value)}
                                />
                            </View>
                            <View >
                                <TextInput placeholder='Masukan nomor telepon' style={{ borderBottomWidth: 2, paddingLeft: 10 }}
                                    value={state.nomor}
                                    onChangeText={nomor => state.handleChangeInputNomor(nomor)}
                                />
                            </View>
                        </View>
                        <TouchableOpacity style={{ backgroundColor: '#623AA2', height: 40, borderRadius: 40, justifyContent: 'center' }} onPress={() => state.addTodo()}>
                            <Text style={{ textAlign: 'center', color: 'white' }}>SIMPAN</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ marginTop: 10, fontSize: 15, fontWeight: 'bold', color: 'white' }}>Daftar Kontak :</Text>
                </View>
                <ScrollView style={{ marginHorizontal: 10 }}>
                    {state.todos.map(todos => {
                        return <Item
                            id={todos.key}
                            text={todos.text}
                            number={todos.number}
                        />
                    })}
                </ScrollView>
            </LinearGradient>
        </View>
    )
}

export default TodoList