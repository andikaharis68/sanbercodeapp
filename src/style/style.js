import { StyleSheet } from 'react-native'
import colors from './colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blue
    },
    textLogoContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.white
    },
    slider: {
        flex: 1
    },
    btnContainer: {
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    btnLogin: {
        height: 35,
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white
    },
    btnTextLogin: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.blue
    },
    btnRegister: {
        height: 35,
        width: '90%',
        borderWidth: 1.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.white,
        backgroundColor: 'transparent'
    },
    btnTextRegister: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    listContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    listContent: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgList: {
        width: 330,
        height: 330
    },
    textList: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    activeDotStyle: {
        width: 20,
        backgroundColor: colors.white
    }
})

export default styles