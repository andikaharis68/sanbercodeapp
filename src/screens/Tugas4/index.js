import React, { createContext, useState } from 'react'
import { View, Text } from 'react-native'
import TodoList from './TodoList'

export const RootContext = createContext()

const Context = () => {
    const [value, setValue] = useState('');
    const [todos, setTodos] = useState([]);

    const handleChangeInput = (value) => {
        setValue(value)
    }
    const addTodo = () => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var today = `${date} / ${month} / ${year}`
        setTodos([...todos, { text: value, date: today, key: Math.random().toString() }]);
        setValue('');
    };
    const deleteTodo = (id) => {
        setTodos(
            todos.filter(todo => {
                if (todo.key != id) return true;
            })
        );
    };
    return (
        <RootContext.Provider value={{
            value,
            todos,
            handleChangeInput,
            deleteTodo,
            addTodo,

        }}>
            <TodoList />
        </RootContext.Provider>
    )
}

export default Context