import React, { useContext } from 'react'
import { View, Text, StatusBar, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import Trash from '../../assets/icons/trash.svg'
import Folder from '../../assets/icons/folder.svg'
import { RootContext } from '../Tugas4'

const TodoList = () => {

    const state = useContext(RootContext)

    const Item = ({ text, date, id }) => {
        return (
            <View style={{ flexDirection: 'row', height: 65, borderWidth: 3, borderColor: 'grey', borderRadius: 5, marginTop: 10, paddingHorizontal: 15, alignItems: 'center' }} >
                <View style={{ flex: 1 }} >
                    <Text >{date}</Text>
                    <Text >{text}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={() => state.deleteTodo(id)} >
                        <Trash heigh={30} width={30} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar backgroundColor="white" />
            <View style={{ marginHorizontal: 10, marginTop: 20 }}>
                <Text style={{ fontSize: 15 }}>Masukan Todolist</Text>
                <View style={{ marginVertical: 10, flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <TextInput placeholder='Input here' style={{ borderWidth: 2, padding: 7 }}
                            value={state.value}
                            onChangeText={value => state.handleChangeInput(value)}
                        />
                    </View>
                    <TouchableOpacity style={{ marginLeft: 10 }} onPress={() => state.addTodo()}>
                        <Folder width={46} height={46} />
                    </TouchableOpacity>
                </View>
                <ScrollView >
                    {state.todos.map(todos => {
                        return <Item
                            id={todos.key}
                            text={todos.text}
                            date={todos.date}
                        />
                    })}
                </ScrollView>
            </View>
        </View>
    )
}

export default TodoList