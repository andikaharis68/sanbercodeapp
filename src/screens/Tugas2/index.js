import * as React from 'react'
import { View, Text, StatusBar, Image } from 'react-native'
import Wallet from '../../assets/icons/wallet.svg'
import Tool from '../../assets/icons/tools.svg'
import Agree from '../../assets/icons/contact-list.svg'
import Hand from '../../assets/icons/handshake.svg'
import Back from '../../assets/icons/exit-door.svg'


const Tugas2 = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#F2F2F2' }}>
            <StatusBar backgroundColor="#0080FF" />
            <View style={{ backgroundColor: '#0080FF', height: 50, justifyContent: 'center' }}>
                <Text style={{ marginLeft: 20, color: 'white', fontSize: 20 }}>Account</Text>
            </View>
            <View style={{ backgroundColor: 'white', height: 90, justifyContent: 'center' }}>
                <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>

                    <Image source={require('../../assets/images/dk.jpg')} style={{ width: 60, height: 60, borderRadius: 60 }} />
                    <Text style={{ marginLeft: 20, fontSize: 18, fontWeight: 'bold' }}>Moch. Andika Haris S</Text>
                </View>
            </View>
            <View style={{ backgroundColor: 'white', height: 70, justifyContent: 'center', marginTop: 1 }}>
                <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                    <Wallet height={30} width={30} />
                    <Text style={{ flex: 1, marginLeft: 20, fontSize: 15 }}>Saldo</Text>
                    <Text style={{}}>Rp. 120.000.000</Text>
                </View>
            </View>
            <View style={{ marginTop: 4 }}>
                <View style={{ backgroundColor: 'white', height: 70, justifyContent: 'center', marginTop: 1 }}>
                    <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Tool height={30} width={30} />
                        <Text style={{ marginLeft: 20, fontSize: 15 }}>Pengaturan</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', height: 70, justifyContent: 'center', marginTop: 1 }}>
                    <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Agree height={30} width={30} />
                        <Text style={{ marginLeft: 20, fontSize: 15 }}>Bantuan</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', height: 70, justifyContent: 'center', marginTop: 1 }}>
                    <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Hand height={30} width={30} />
                        <Text style={{ marginLeft: 20, fontSize: 15 }}>Syarat & Ketentuan</Text>
                    </View>
                </View>
            </View>
            <View style={{ backgroundColor: 'white', height: 70, justifyContent: 'center', marginTop: 5 }}>
                <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                    <Back height={30} width={30} />
                    <Text style={{ marginLeft: 20, fontSize: 15 }}>Keluar</Text>
                </View>
            </View>

        </View>
    )
}

export default Tugas2;