import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import Trash from '../../assets/icons/trash.svg'
import Folder from '../../assets/icons/folder.svg'


const Tugas3 = () => {
    const [value, setValue] = useState('');
    const [todos, setTodos] = useState([]);
    const [currentDate, setCurrentDate] = useState('');

    useEffect(() => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        setCurrentDate(
            date + '/' + month + '/' + year
        );
    }, []);


    const addTodo = () => {
        setTodos([...todos, { text: value, date: currentDate, key: Math.random().toString() }]);
        setValue('');
    };
    const deleteTodo = (id) => {
        setTodos(
            todos.filter(todo => {
                if (todo.key != id) return true;
            })
        );
    };

    const Item = ({ text, date, id }) => {
        return (
            <View style={{ flexDirection: 'row', height: 65, borderWidth: 3, borderColor: 'grey', borderRadius: 5, marginTop: 10, paddingHorizontal: 15, alignItems: 'center' }} >
                <View style={{ flex: 1 }} >
                    <Text >{date}</Text>
                    <Text >{text}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={() => deleteTodo(id)} >
                        <Trash heigh={30} width={30} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar backgroundColor="white" />
            <View style={{ marginHorizontal: 10, marginTop: 20 }}>
                <Text style={{ fontSize: 15 }}>Masukan Todolist</Text>
                <View style={{ marginVertical: 10, flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <TextInput placeholder='Input here' style={{ borderWidth: 2, padding: 7 }}
                            value={value}
                            onChangeText={value => setValue(value)}
                        />
                    </View>
                    <TouchableOpacity style={{ marginLeft: 10 }} onPress={() => addTodo()}>
                        <Folder width={46} height={46} />
                    </TouchableOpacity>
                </View>
                <ScrollView >
                    {todos.map(todos => {
                        return <Item
                            id={todos.key}
                            text={todos.text}
                            date={todos.date}
                        />
                    })}
                </ScrollView>
            </View>
        </View>
    )
}

export default Tugas3