import React from 'react';
import Tugas1 from './src/screens/Tugas1/tugas1';
import Tugas2 from './src/screens/Tugas2/index';
import Tugas3 from './src/screens/Tugas3/TodoList'
import Tugas4 from './src/screens/Tugas4'
import Quiz1 from './quiz1/index'
import AppNavigation from './src/navigations/routes'

const App = () => {
  return (
    <AppNavigation />
  )
}
export default App